#!/bin/sh
BASEDIR=$(readlink -f "$(dirname "$0")")

mkdir -p "${BASEDIR}/.paccache"

CONTAINER=$(podman run -d -it --rm --userns=keep-id \
    -w"${PWD}" \
    -v"${BASEDIR}:${BASEDIR}" \
    -v"${BASEDIR}/.paccache:/var/cache/pacman/pkg" \
    -v"/etc/pacman.d/mirrorlist:/etc/pacman.d/mirrorlist:ro" \
    docker.io/library/archlinux:latest bash)

podman exec -i --user=root "${CONTAINER}" sh <<EOF
pacman -Syu --noconfirm sudo base-devel git
echo "ALL ALL=(root) NOPASSWD: /usr/bin/pacman" >> /etc/sudoers
EOF

podman attach "${CONTAINER}"
